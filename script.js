let notNumber = "It's not a number.";
let notSymbol = "It's not a mathimatical symbol that I know. (I only know +, -, *, /.)";
let tryAgain = "Please, try again.";
let firstNumberPhrase = "Input your first number, please!";
let secondNumberPhrase = "Input your second number, please!";
let operationPhrase = "Input the mathematical symbol, please! (I only know +, -, *, /.)";


let firstNumber = prompt(firstNumberPhrase);
firstNumber = checkNumber(firstNumber);

let secondNumber = prompt(secondNumberPhrase);
secondNumber = checkNumber(secondNumber);

let operation = prompt(operationPhrase);
operation = checkOperation(operation);
mathematicalOperation(operation, Number(firstNumber), Number(secondNumber));


function checkNumber(checkNum) {
	while (isNaN(Number(checkNum))) {
		checkNum = prompt(notNumber + " " + tryAgain, checkNum);
	}
    return checkNum;
}

function checkOperation(checkOper) {
	while (checkOper != "+" && checkOper != "-" && checkOper != "*" && checkOper != "/") {
        checkOper = prompt(notSymbol, checkOper);
    }
    return checkOper;
}

function mathematicalOperation(mathOperation, num1, num2) {
	switch (mathOperation) {
        case "+":
            console.log(num1 + num2);
            break;
        case "-":
            console.log(num1 - num2);
            break;
        case "*":
            console.log(num1 * num2);
            break;
        case "/":
            console.log(num1 / Number(num2)); 
            break;
    }
}